using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MVC_CodeFirst.Models;
using MVC_CodeFirst.Services;
using MVC_CodeFirst.Services.Repository;
using MVC_CodeFirst.Services.Repository.Base;
using MVC_CodeFirst.Services.Repository.Context;
using MVC_CodeFirst.Services.Base;
using MVC_CodeFirst.Services.Services;
using MVC_CodeFirst.Services.ServicesContracts;
using System.Data.Entity;

namespace MVC_CodeFirst.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //InitAutoFac();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        //private void InitAutoFac()
        //{
        //    var builder = new ContainerBuilder();

        //    //Repository
        //    builder.RegisterType<UserRepository>().AsSelf().As<IRepository<UserModel>>();

        //    //Service
        //    builder.RegisterType<UserService>().AsSelf().As<IService<UserModel>>().As<IUserService>();

        //    builder.RegisterType<MVC_CodeFirst_Database>().As<DbContext>().InstancePerLifetimeScope();
        //}
    }
}
