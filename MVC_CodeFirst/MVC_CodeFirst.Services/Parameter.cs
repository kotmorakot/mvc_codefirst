﻿using System;
using System.Collections.Generic;

namespace MVC_CodeFirst.Services
{
    public class Parameter
    {
        public string storeprocedure { get; set; }
        public Dictionary<string, string> parameter { get; set; }
    }
}
