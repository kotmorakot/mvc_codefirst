﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace MVC_CodeFirst.Services.Repository.Base
{
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;

        public RepositoryBase(DbContext context)
        {
            _context = context;
        }

        //public RepositoryBase()
        //{
        //    _context = new DbContext("Data Source=.;Initial Catalog=TCM;Integrated Security=True;");
        //}
        public T Add(T item)
        {
            return _context.Set<T>().Add(item);
        }

        public IQueryable<T> Query(Func<T, bool> criteria)
        {
            return _context.Set<T>().Where(criteria).AsQueryable();
        }

        public T Remove(T item)
        {
            return _context.Set<T>().Remove(item);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public IQueryable<T> All()
        {
            return _context.Set<T>().AsQueryable();
        }
        public IEnumerable<T> Include(params Expression<Func<T, object>>[] includes)
        {
            IDbSet<T> dbSet = _context.Set<T>();

            IEnumerable<T> query = null;
            foreach (var include in includes)
            {
                query = dbSet.Include(include);
            }

            return query ?? dbSet;
        }


        public DataTable CallStore(Parameter parameter)
        {
            SqlParameter[] parameters = null;
            if (parameter.parameter != null)
            {
                parameters = new SqlParameter[parameter.parameter.Count];
                int c = 0;
                foreach (var param in parameter.parameter)
                {
                    parameters[c] = new SqlParameter(param.Key, param.Value);
                    c++;
                }
            }


            string strCon = _context.Database.Connection.ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = parameter.storeprocedure;
            if (parameter.parameter != null)
                cmd.Parameters.AddRange(parameters);
            con.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            var dataTable = new DataTable();
            dataTable.Load(reader);
            con.Close();

            return dataTable;

        }

        public DataSet CallStore(Parameter parameter, bool multipleTable)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter reader = new SqlDataAdapter();

            SqlParameter[] parameters = null;
            if (parameter.parameter != null)
            {
                parameters = new SqlParameter[parameter.parameter.Count];
                int c = 0;
                foreach (var param in parameter.parameter)
                {
                    parameters[c] = new SqlParameter(param.Key, param.Value);
                    c++;
                }
            }

            string strCon = _context.Database.Connection.ConnectionString;
            SqlConnection con = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = parameter.storeprocedure;
            if (parameter.parameter != null)
                cmd.Parameters.AddRange(parameters);
            con.Open();

            reader = new SqlDataAdapter(cmd);
            reader.Fill(ds);
            con.Close();

            return ds;

        }
    }
}
