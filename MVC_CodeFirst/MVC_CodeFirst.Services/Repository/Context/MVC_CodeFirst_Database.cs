﻿using MVC_CodeFirst.Models;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
//DatabaseConnection
namespace MVC_CodeFirst.Services.Repository.Context
{
    public class MVC_CodeFirst_Database : DbContext
    {
        public MVC_CodeFirst_Database() : base("DatabaseConnection")
        {
            // use add-migration "name"
            // use update-database
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            objectContext.SavingChanges += (sender, args) =>
            {
                var now = DateTime.Now;
                foreach (var entry in ChangeTracker.Entries<IModel>())
                {
                    var entity = entry.Entity;
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entity.CreatedDate = now;
                            entity.UpdateDate = now;
                            break;
                        case EntityState.Modified:
                            entity.UpdateDate = now;
                            break;
                    }
                }
                ChangeTracker.DetectChanges();
            };
        }

        public DbSet<UserModel> users { get; set; }
        
    }
    //{
    //    // static  string con = "Data Source=.;Initial Catalog=TCM;Integrated Security=True;";

    //    public MVC_CodeFirst_Database() : base(GetSqlConnection("DatabaseConnection"))
    //    {

    //        // use add-migration "name"
    //        // use update-database
    //        var objectContext = ((IObjectContextAdapter)this).ObjectContext;
    //        objectContext.SavingChanges += (sender, args) =>
    //        {
    //            var now = DateTime.Now;
    //            foreach (var entry in ChangeTracker.Entries<IModel>())
    //            {
    //                var entity = entry.Entity;
    //                switch (entry.State)
    //                {
    //                    case EntityState.Added:
    //                        entity.CreatedDate = now;
    //                        entity.UpdateDate = now;
    //                        break;
    //                    case EntityState.Modified:
    //                        entity.UpdateDate = now;
    //                        break;
    //                }
    //            }
    //            ChangeTracker.DetectChanges();
    //        };
    //    }

    //    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //    {

    //        //modelBuilder.Entity<ProgramModel>()
    //        //    .HasRequired(p => p.User)
    //        //    .WithMany()
    //        //    .WillCascadeOnDelete(false);

    //    }

    //    #region Get connection string

    //    public static string GetSqlConnection(string connectionStringName = "DefaultConnection")
    //    {
    //        //string p1 = EncryptionHelper.EncryptStringAES("cpdstcmprdusr");
    //        // optionally defaults to "DefaultConnection" if no connection string name is inputted
    //        string connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
    //        // decrypt password
    //        //string password = get_prase_after_word(connectionString, "password=", ";");
    //        //if (!string.IsNullOrEmpty(password))
    //        //    connectionString = connectionString.Replace(password, EncryptionHelper.DecryptStringAES(password));
    //        // return connectionString;
    //        return "DatabaseConnection";
    //    }

    //    public static string get_prase_after_word(string search_string_in, string word_before_in, string word_after_in)
    //    {
    //        int myStartPos = 0;
    //        string myWorkString = "";

    //        // get position where phrase "word_before_in" ends

    //        if (!string.IsNullOrEmpty(word_before_in))
    //        {
    //            myStartPos = search_string_in.ToLower().IndexOf(word_before_in) + word_before_in.Length;

    //            // extract remaining text
    //            myWorkString = search_string_in.Substring(myStartPos, search_string_in.Length - myStartPos).Trim();

    //            if (!string.IsNullOrEmpty(word_after_in))
    //            {
    //                // get position where phrase starts in the working string
    //                myWorkString = myWorkString.Substring(0, myWorkString.IndexOf(word_after_in)).Trim();

    //            }
    //        }
    //        else
    //        {
    //            myWorkString = string.Empty;
    //        }
    //        return myWorkString.Trim();
    //    }

    //    #endregion

    //    //public DbSet<UserModel> users { get; set; }

    //}
}
