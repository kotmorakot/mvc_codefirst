﻿using System.Data.Entity;
using MVC_CodeFirst.Models;
using MVC_CodeFirst.Services.Repository.Base;

namespace MVC_CodeFirst.Services.Repository {
	
    public class UserRepository : RepositoryBase<UserModel>
    {
        public UserRepository(DbContext context) : base(context)
        {
        }
    }
}



