﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;


namespace MVC_CodeFirst.Services
{
    class EncryptionHelper
    {
        private const string EncryptionKey = "MVC_CodeFirst";
        private static byte[] salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

        public static string EncryptStringAES(string plainText)
        {
            string outStr = null;                       // Encrypted string to return         
            RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.          
            try
            {
                // generate the key from the shared secret and the salt             
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(EncryptionKey, salt);
                // Create a RijndaelManaged object             
                // with the specified key and IV.             
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
                // Create a decrytor to perform the stream transform.             
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for encryption.             
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.                         
                            swEncrypt.Write(plainText);
                        }
                    }
                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.             
                if (aesAlg != null)
                    aesAlg.Clear();
            }
            // Return the encrypted bytes from the memory stream.         
            return outStr;
        }

        public static string DecryptStringAES(string cipherText)
        {
            // Declare the RijndaelManaged object         
            // used to decrypt the data.         
            RijndaelManaged aesAlg = null;
            // Declare the string used to hold         
            // the decrypted text.         
            string plaintext = null;
            try
            {
                // generate the key from the shared secret and the salt             
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(EncryptionKey, salt);
                // Create a RijndaelManaged object             
                // with the specified key and IV.             
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
                // Create a decryptor to perform the stream transform.             
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for decryption.                            
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            // Read the decrypted bytes from the decrypting stream                        
                            // and place them in a string.                         
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            catch
            {
                return cipherText;
            }
            finally
            {
                // Clear the RijndaelManaged object.             
                if (aesAlg != null)
                    aesAlg.Clear();
            }
            return plaintext;
        }

    }
}
