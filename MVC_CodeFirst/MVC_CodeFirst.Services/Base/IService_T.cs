﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace MVC_CodeFirst.Services.Base
{
    public interface IService<T> where T : class
    {
        T Find(params object[] keys);

        IQueryable<T> All();
        IQueryable<T> Query(Func<T, bool> criteria);
        T Add(T item);
        T Remove(T item);

        int SaveChanges();

        IEnumerable<T> Include(params Expression<Func<T, object>>[] includes);

        DataTable CallStore(Parameter parameter);
        DataSet CallStore(Parameter parameter, bool multipleTable);
    }
}
