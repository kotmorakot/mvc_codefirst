﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using MVC_CodeFirst.Services.Repository.Base;

namespace MVC_CodeFirst.Services.Base
{
    public abstract class ServiceBase<T> : IService<T> where T : class
    {
        private readonly IRepository<T> _BaseRepo;

        public ServiceBase(IRepository<T> baseRepo)
        {
            _BaseRepo = baseRepo;
        }

        public abstract T Find(params object[] keys);

        public virtual T Add(T item) => _BaseRepo.Add(item);

        public IQueryable<T> All() => Query(_ => true);

        public IQueryable<T> Query(Func<T, bool> criteria) => _BaseRepo.Query(criteria);

        public T Remove(T item) => _BaseRepo.Remove(item);

        public int SaveChanges() => _BaseRepo.SaveChanges();

        public IEnumerable<T> Include(params Expression<Func<T, object>>[] includes) => _BaseRepo.Include(includes);

        public DataTable CallStore(Parameter parameter) => _BaseRepo.CallStore(parameter);
        public DataSet CallStore(Parameter parameter, bool multipleTable) => _BaseRepo.CallStore(parameter, multipleTable);
    }
}