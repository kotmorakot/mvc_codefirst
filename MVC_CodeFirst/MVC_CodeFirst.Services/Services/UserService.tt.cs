﻿using System;
using System.Linq;
using MVC_CodeFirst.Models;
using MVC_CodeFirst.Services.Base;
using MVC_CodeFirst.Services.Repository.Base;
using MVC_CodeFirst.Services.ServicesContracts;

namespace MVC_CodeFirst.Services.Services {
	
	public class UserService : ServiceBase<UserModel>, IUserService
    {
		public UserService(IRepository<UserModel> baseRepo) : base(baseRepo)
        {
        }

		public override UserModel Find(params object[] keys)
        {
            var pk = Convert.ToInt32(keys[0].ToString());

            return Query(x => x.PK == pk).SingleOrDefault();
        }

        public UserModel FindById(int pk)
        {
            return Query(x => x.PK == pk).SingleOrDefault();
        }

        public override UserModel Add(UserModel item)
        {
            ValidateBeforeSave(item);

            return base.Add(item);
        }
        private bool ValidateBeforeSave(UserModel item)
        {
            return true;
        }
	}
}



