﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MVC_CodeFirst.Models {
	
	[Table("User")]
    public class UserModel : IModel {
	[Key]
		
		[Required]
		public int PK {get; set;}
		
		[Required]
		[Display(Name = "UserName")]
		[StringLength(255)]
		public string UserName {get; set;}
		
		[Required]
		[Display(Name = "PassWord")]
		[StringLength(4000)]
		public string PassWord {get; set;}
		public DateTime? CreatedDate { get; set; }
		public int CreateBy { get; set; }
		public DateTime? UpdateDate { get; set; }
		public int UpdateBy { get; set; }
	}
}



