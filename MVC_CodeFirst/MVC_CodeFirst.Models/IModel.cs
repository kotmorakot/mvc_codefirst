﻿using System;

namespace MVC_CodeFirst.Models
{
    public interface IModel
    {
        DateTime? CreatedDate { get; set; }
        int CreateBy { get; set; }
        DateTime? UpdateDate { get; set; }
        int UpdateBy { get; set; }

    }
}
